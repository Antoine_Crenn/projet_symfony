<?php

namespace App\DataFixtures;

use App\Entity\Contrat;
use App\Entity\Offre;
use App\Entity\TypeContrat;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class OffreFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $cdd = new Contrat();
        $cdd->setTitre("CDD");
        $manager->persist($cdd);
        $cdi = new Contrat();
        $cdi->setTitre("CDI");
        $manager->persist($cdi);
        $freelance = new Contrat();
        $freelance->setTitre("Freelance");
        $manager->persist($freelance);
        $temps_plein = new TypeContrat();
        $temps_plein->setTitre("Temps Plein");
        $manager->persist($temps_plein);
        $temps_partiel = new TypeContrat();
        $temps_partiel->setTitre("Temps Partiel");
        $manager->persist($temps_partiel);

        for ($index = 0; $index < 25; $index++) {
            $contrat = $faker->randomElement([$cdd, $cdi, $freelance]);
            $type_contrat = $faker->randomElement([$temps_plein, $temps_partiel]);
            $offre = new Offre();
            $ville = $faker->city();
            $code_postal = $faker->postcode();
            $nom_rue = $faker->streetAddress();
            $adresse = $nom_rue . ', ' . $code_postal . ', ' . $ville;
            $offre->setTitre($faker->jobTitle())
                ->setDescription($faker->sentence(10, true))
                ->setCodePostal($code_postal)
                ->setVille($ville)
                ->setAdresse($adresse)
                ->setDateCreation($faker->dateTimeBetween('-6 months', '- 3 months', 'Europe/Paris'))
                ->setDateMiseAJour($faker->dateTimeBetween('-2 months', 'now', 'Europe/Paris'))
                ->setContrat($contrat)
                ->setTypeContrat($type_contrat);
            if ($contrat->getTitre() == "CDD" or $contrat->getTitre() == "Freelance") {
                $offre->setFinMission($faker->dateTimeBetween('+1 months', '+6 months', 'Europe/Paris'));
            } else {
                $offre->setFinMission(Null);
            }

            $manager->persist($offre);
        }
        $manager->flush();
    }
}
