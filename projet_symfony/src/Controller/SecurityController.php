<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\RegistrationFormType;
use App\Security\ConnexionAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $outils): Response
    {
        $erreur = $outils->getLastAuthenticationError();
        $nom_utilisateur_precedant = $outils->getLastUsername();

        return $this->render('security/login.html.twig', ['nom_utilisateur' => $nom_utilisateur_precedant, 'erreur' => $erreur]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('Cette méthode peut être vide - elle sera interceptée par la clé de déconnexion sur votre pare-feu.');
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $requete, UserPasswordEncoderInterface $encode, GuardAuthenticatorHandler $garde, ConnexionAuthenticator $indentification, EntityManagerInterface $manager_interface): Response
    {
        $utilisateur = new Utilisateur();
        $formulaire = $this->createForm(RegistrationFormType::class, $utilisateur);
        $formulaire->handleRequest($requete);

        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            // encode the plain password
            $utilisateur->setPassword(
                $encode->encodePassword(
                    $utilisateur,
                    $formulaire->get('plainPassword')->getData()
                )

            );

            $manager_interface->persist($utilisateur);
            $manager_interface->flush();

            return $garde->authenticateUserAndHandleSuccess(
                $utilisateur,
                $requete,
                $indentification,
                'main'
            );
            return $this->redirect('/');
        }

        return $this->render('security/register.html.twig', [
            'formulaire' => $formulaire->createView(),
        ]);
    }
}
