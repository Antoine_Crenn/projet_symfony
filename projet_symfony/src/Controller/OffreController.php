<?php

namespace App\Controller;

use App\Entity\Offre;
use App\Form\FormulaireType;
use App\Repository\ContratRepository;
use App\Repository\OffreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OffreController extends AbstractController
{
    /**
     * @Route("/", name="offre")
     */
    public function index(OffreRepository $repertoire): Response
    {
        $offres = $repertoire->findAll();

        return $this->render('offre/index.html.twig', [
            'offres' => $offres
        ]);
    }

    /**
     * @Route("/gestion", name="offre.gestion")
     */
    public function gestion(OffreRepository $repertoire): Response
    {
        $offres = $repertoire->findAll();

        return $this->render('offre/gestion.html.twig', [
            'offres' => $offres
        ]);
    }

    /**
     * @Route("/gestion/{id}", name="offre.modification")
     */
    public function modification_offre(Offre $offre, Request $requete, EntityManagerInterface $manager_interface): Response
    {

        $formulaire = $this->createForm(FormulaireType::class, $offre, [
            "attr" => [
                "class" => "formulaire_modification_offre",
            ]
        ]);

        $formulaire->handleRequest($requete);
        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            $manager_interface->flush();
            return $this->redirect('/gestion');
        }

        return $this->render('offre/modification.html.twig', [
            'formulaire' => $formulaire->createView(),
            'offre' => $offre,
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="offre.supprimer")
     */
    public function supprimer_offre(Offre $offre, EntityManagerInterface $manager_interface): Response
    {
        $manager_interface->remove($offre);
        $manager_interface->flush();
        return $this->redirect('/gestion');
    }

    /**
     * @Route("/formulaire", name="offre.formulaire")
     */
    public function formulaire(ContratRepository $contrat, Request $requete, EntityManagerInterface $manager_interface): Response
    {
        $cdi = $contrat->findOneBy(["titre" => "CDI"]);
        $offre = new Offre();

        $formulaire = $this->createForm(FormulaireType::class, $offre, [
            "attr" => [
                "class" => "formulaire_offre",
            ]
        ]);

        if (empty($offre->getDateCreation())) {
            $offre->setDateCreation(new \DateTime('now'));
        } else {
            $offre->setDateMiseAJour(new \DateTime('now'));
        }
        $formulaire->handleRequest($requete);
        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            if ($offre->getContrat() == $cdi) {
                $offre->setFinMission(null);
            }
            $manager_interface->persist($offre);
            $manager_interface->flush();
            return $this->redirect('/gestion');
        }

        return $this->render('offre/formulaire.html.twig', [
            'formulaire' => $formulaire->createView(),
        ]);
    }

    /**
     * @Route("/offre/{id}", name="offre.show")
     */
    public function show(Offre $offre): Response
    {
        return $this->render('offre/offre.html.twig', [
            'offre' => $offre,
        ]);
    }
}
