<?php

namespace App\Form;

use App\Entity\Offre;
use App\Repository\ContratRepository;
use App\Repository\TypeContratRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormulaireType extends AbstractType
{

    public function __construct(ContratRepository $contrat, TypeContratRepository $type_contrat)
    {
        $this->cdd = $contrat;
        $this->cdi = $contrat;
        $this->freelance = $contrat;
        $this->temps_plein = $type_contrat;
        $this->temps_partiel = $type_contrat;
    }

    public function buildForm(FormBuilderInterface $builder,  array $options)
    {

        $cdd = $this->cdd->findOneBy(["titre" => "CDD"]);
        $cdi = $this->cdi->findOneBy(["titre" => "CDI"]);
        $freelance = $this->freelance->findOneBy(["titre" => "Freelance"]);
        $temps_plein = $this->temps_plein->findOneBy(["titre" => "Temps Plein"]);
        $temps_partiel = $this->temps_partiel->findOneBy(["titre" => "Temps Partiel"]);

        $builder
            ->add("titre", TextType::class, [
                "attr" => [
                    "class" => "input_titre",
                    "placeholder" => "Titre de l'Offre"
                ]
            ])
            ->add("description", TextareaType::class, [
                "attr" => [
                    "class" => "input_description",
                    "placeholder" => "Description de l'Offre"
                ]
            ])
            ->add("adresse", TextType::class, [
                "attr" => [
                    "class" => "input_adresse",
                    "placeholder" => "Adresse de l'Offre"
                ]
            ])
            ->add("ville", TextType::class, [
                "attr" => [
                    "class" => "input_ville",
                    "placeholder" => "Ville de l'Offre"
                ]
            ])
            ->add("code_postal", TextType::class, [
                "attr" => [
                    "class" => "input_code_postal",
                    "placeholder" => "Code Postal de l'Offre"
                ]
            ])
            ->add("contrat", ChoiceType::class, [
                'choices' => [
                    'CDI' => $cdi,
                    'CDD' => $cdd,
                    'Freelance' => $freelance,
                ],
            ], [
                "attr" => [
                    "class" => "input_contrat",
                    "value" => $cdi,
                ]
            ])
            ->add("fin_mission", DateType::class, [
                "attr" => [
                    "class" => "input_fin_mission",
                ]
            ], ['required' => false])
            ->add("type_contrat", ChoiceType::class, [
                'choices' => [
                    'Temps Plein' => $temps_plein,
                    'Temps Partiel' => $temps_partiel,
                ],
            ], [
                "attr" => [
                    "class" => "input_type_contrat",
                ]
            ])
            ->add("envoyer", SubmitType::class, [
                "attr" => [
                    "role" => "button",
                    "class" => "btn bouton",
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offre::class,
        ]);
    }
}
