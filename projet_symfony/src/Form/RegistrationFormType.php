<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                "attr" => [
                    "class" => "input_email",
                    "placeholder" => "Email"
                ]
            ])
            ->add('prenom', TextType::class, [
                "attr" => [
                    "class" => "input_prenom",
                    "placeholder" => "Prénom"
                ]
            ])
            ->add('nom_famille', TextType::class, [
                "attr" => [
                    "class" => "input_nom_famille",
                    "placeholder" => "Nom de Famille"
                ]
            ])
            ->add('nom_utilisateur', TextType::class, [
                "attr" => [
                    "class" => "input_nom_utilisateur",
                    "placeholder" => "Nom Utilisateur"
                ]
            ])
            ->add('plainPassword', PasswordType::class, [
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        'max' => 4096,
                    ]),
                ],
                "attr" => [
                    "class" => "input_mot_passe",
                    "placeholder" => "Mot de Passe"
                ]
            ])
            ->add("Inscription", SubmitType::class, [
                "attr" => [
                    "role" => "button",
                    "class" => "btn bouton",
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
